var slides = [];
var slide_id;

function initSliderShow() {
    var slidershow = document.getElementById("slidershow");
    var slides = slidershow.getElementsByClassName("slide");

    // Add the slides indicator
    var gindicator = document.getElementById("slidershow-indicator");
    for (var i=0; i<slides.length; i++) {
        var indicator = document.createElement('div');
        indicator.className = "slide-indicator empty";
        gindicator.appendChild(indicator);
    }

    var indicators = slidershow.getElementsByClassName("slide-indicator");

    function next() {
        if (slide_id === undefined) {
            // We have not an enabled slide yet
            slide_id = -1;
        }
        else {
            if (slide_id == slides.length - 1) {
                return;
            }
            var slide = slides[slide_id];
            slide.classList.remove('center');    
            slide.classList.remove('right');    
            slide.classList.add('left');
            var ind = indicators[slide_id];
            ind.classList.remove('fill');    
            ind.classList.add('empty');
        }
        slide_id += 1;
        var slide = slides[slide_id];
        slide.classList.remove('left');    
        slide.classList.remove('right');    
        slide.classList.add('center');
        var ind = indicators[slide_id];
        ind.classList.remove('empty');    
        ind.classList.add('fill');
    }

    function prev() {
        if (slide_id === undefined) {
            // We have not an enabled slide yet
            slide_id = -1;
        }
        else {
            if (slide_id == 0) {
                return;
            }
            var slide = slides[slide_id];
            slide.classList.remove('center');    
            slide.classList.remove('left');    
            slide.classList.add('right');
            var ind = indicators[slide_id];
            ind.classList.remove('fill');    
            ind.classList.add('empty');
        }
        slide_id -= 1;
        var slide = slides[slide_id];
        slide.classList.remove('left');
        slide.classList.remove('right');
        slide.classList.add('center');
        var ind = indicators[slide_id];
        ind.classList.remove('empty');    
        ind.classList.add('fill');
    }

    // Show the first slide
    next();

    // Keyboard navigation
    $(window).keydown(function( event ) {
        if ( event.which == 37 ) {
            event.preventDefault();
            prev();
        }
        else if ( event.which == 39 ) {
            event.preventDefault();
            next();
        }
    });

    // Wheel mouse navigation
    $(window).mousewheel(function(turn, delta) {
        if (delta == 1) {
            prev();
        }
        else {
            next();
        }
        return false;
    });

    var startX,
        startY,
        dist,
        threshold = 30, //required min distance traveled to be considered swipe
        allowedTime = 300, // maximum time allowed to travel that distance
        onswipe = false,
        elapsedTime,
        startTime;
    slidershow.addEventListener('touchstart', function(e){
        //touchsurface.innerHTML = ''
        var touchobj = e.changedTouches[0];
        dist = 0;
        startX = touchobj.pageX;
        startY = touchobj.pageY;
        startTime = new Date().getTime(); // record time when finger first makes contact with surface
        onswipe = true;
        e.preventDefault();
    }, false);

    slidershow.addEventListener('touchmove', function(e){
        e.preventDefault(); // prevent scrolling when inside DIV
    }, false);

    slidershow.addEventListener('touchend', function(e){
        if (!onswipe) {
            return;
        }
        onswipe = false;
        var touchobj = e.changedTouches[0];
        dist = touchobj.pageX - startX; // get total dist traveled by finger while in contact with surface
        elapsedTime = new Date().getTime() - startTime; // get time elapsed
        // check that elapsed time is within specified, horizontal dist traveled >= threshold, and vertical dist traveled <= 100
        var swiperightBol = (elapsedTime <= allowedTime && Math.abs(dist) >= threshold && Math.abs(touchobj.pageY - startY) <= 100);
        
        if(swiperightBol){
            if(dist < 0){
                next();
            }else{
                prev();
            }
        }
        
        e.preventDefault();
    }, false);
}; 
